var gulp          = require('gulp');
var concat        = require('gulp-concat');
const rename      = require('gulp-rename');
var fs            = require("fs");
var podName       = require('./package.json').name;
var trioFuncName  = podName + '.trio'
const trioNewLine = "\r\n\r\n---\r\n";

gulp.task('trio', function concatTrio(done) {
  // build a single trio file out of all of our trio files
  return gulp.src("./trio/*.trio")
    .pipe(concat(trioFuncName, { newLine: trioNewLine})) .pipe(gulp.dest("./lib"));
});

gulp.task('unitTest', function concatUnitTrio(done) {
  // build a single trio file out of all of our trio files
  return gulp.src("./test/unitTests/*.trio")
    .pipe(concat("unitTest-" + trioFuncName, { newLine: trioNewLine}))
    .pipe(gulp.dest( "./lib"));
});

gulp.task('apps', function() {
  // build a single trio file out of all of our trio files
  return gulp.src("./apps/*.trio")
    .pipe(concat("apps.trio", { newLine: trioNewLine}))
    .pipe(gulp.dest( "./lib"));
});

gulp.task('views', function() {
  // build a single trio file out of all of our trio files
  return gulp.src("./views/*.trio")
    .pipe(concat("views.trio", { newLine: trioNewLine}))
    .pipe(gulp.dest( "./lib"));
});

gulp.task('defs', function() {
  return gulp.src("./defs/*.trio")
    .pipe(concat("defs.trio", { newLine: trioNewLine}))
    .pipe(gulp.dest( "./lib"));
});


gulp.task('templates', function() {
  // build a single trio file out of all of our trio files
  return gulp.src("./templates/*.trio")
    .pipe(concat("templates.trio", { newLine: trioNewLine}))
    .pipe(gulp.dest( "./lib"));
});

gulp.task('default', gulp.series('trio','unitTest', 'apps', 'views', 'defs', 'templates'));
gulp.task('noTestInPod', gulp.series('trio', 'unitTest', 'apps', 'views', 'defs', 'templates'));
