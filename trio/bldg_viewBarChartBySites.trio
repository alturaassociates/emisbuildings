name:bldg_viewBarChartBySites
bldgPod
doc:
  Return grid by matching site, energy metric and energy types. When the input parameter energyType is "Total", the function calculates the total.
  
  Inputs
  - selectedSites: Input accepted by
  - utilityMetric: Str
  - utilityType: Str
  - monthlyToggleStatus: Bool
  - showPrevYear: Bool
  
  Outputs
  - grid
  
  
  Example
  
      do
      selectedSites:readAll(bldgCompare and site);
      bldg_viewBarChartBySites(selectedSites, "Consumption", "Electric", true, false);
       bldg_viewBarChartBySites(selectedSites, "Consumption", Total", true, false);
    end
  
  
      do
        selectedSites:readAll(bldgCompare and site);
        bldg_viewBarChartBySites(selectedSites, "Consumption", "Steam", true, false);
      end
  
  Authors
  - Gia Nguyen: document, test, and enhancements
  - Hali Sanderlin: original author
  
  
  ©Altura Associates 2023
func
src:
  (dates, utilityMetric, utilityType, monthlyToggleStatus, showPrevYear, sitesA,
  bldgSnapshotRecsA: readAll(bldgSnapshotRec and not execKpiWorkEventData))=> do
    //- - - -//
    helper_keepColsWhenFoldPositive: (gridA, colsCheck, keepOtherCols: true)=>do
      util_assert([], "Input colsCheck is a list of column names", colsCheck.isList and colsCheck.all(c=>c.isStr))
      colsSatisfyConditions: colsCheck.findAll(c=> gridA.has(c) and gridA.foldCol(c, sum)>0)
  
      gridB: null
      if (keepOtherCols) do
        otherCols: gridA.colNames.findAll(c=> not colsCheck.contains(c))
        colsComb: otherCols.addAll(colsSatisfyConditions)
        gridB = gridA.keepCols(colsComb)
      else do
          gridB = gridA.keepCols(colsSatisfyConditions)
      end
  
      gridB
    end
  
    helper_addMetaToCols: (gridA, dictMeta)=>do
      /*
      Assume the following structure
      gridA:
       col1   col2   col3 ...
  
     dictMeta: {col1: {dis: "", color: },
                 col2: {dis: "", color: }}
      */
      gridB: gridA
      colsA: gridA.colNames
      dictMeta.each((v, k)=> if (colsA.contains(k)) gridB = gridB.addColMeta(k, dictMeta.trap(k)))
      gridB
    end
  
    helper_sumSnapshotRecsAndPutIn: (bldgSnapshotRecs, tagA, tagPutIn, opts: {})=>do
      sumRes: bldg_sumSnapshotRecs(bldgSnapshotRecs, tagA, opts)
      out: if (not isEmpty(sumRes) and sumRes.has(tagA))
              set({}, tagPutIn, sumRes.trap(tagA))
            else
              {}
      out
    end
    //- - - -//
    if (isEmpty(bldgSnapshotRecsA))
      return {dis: "Input bldgSnapshotRecsA is empty"}
  
    siteIdsA: toRecIdList(sitesA)
    colorGrid: bldg_gridUtilityTypeColors()
    colorDict: {electric: colorGrid.find(me => me->utilityType=="Electric")->color,
                gas: colorGrid.find(me => me->utilityType=="Gas")->color,
                chw: colorGrid.find(me => me->utilityType=="Chilled Water")->color,
                hhw: colorGrid.find(me => me->utilityType=="Hot Water")->color,
                steam: colorGrid.find(me => me->utilityType=="Steam")->color,
                water: colorGrid.find(me => me->utilityType=="Water")->color,
                }
  
    colorAndDis:
            {"elec_eui":{dis: "Electric EUI", color: colorDict->electric},
              "gas_eui": {dis: "Gas EUI", color: colorDict->gas},
              "chw_eui": {dis: "Chilled Water EUI", color: colorDict->chw},
              "hhw_eui": {dis: "Hot Water EUI", color: colorDict->hhw},
              "steam_eui": {dis: "Steam EUI", color: colorDict->steam},
  
              "elec_energy":{dis: "Electric Energy", color: colorDict->electric},
              "gas_energy": {dis: "Gas Energy", color: colorDict->gas},
              "chw_energy": {dis: "Chilled Water Energy", color: colorDict->chw},
              "hhw_energy": {dis: "Hot Water Energy", color: colorDict->hhw},
              "steam_energy": {dis: "Steam Energy", color: colorDict->steam},
  
                "elec_cost":{dis: "Electric Cost", color: colorDict->electric},
              "gas_cost": {dis: "Gas Cost", color: colorDict->gas},
              "chw_cost": {dis: "Chilled Water Cost", color: colorDict->chw},
              "hhw_cost": {dis: "Hot Water Cost", color: colorDict->hhw},
              "steam_cost": {dis: "Steam Cost", color: colorDict->steam},
              "water_cost": {dis: "Water Cost", color: colorDict->water},
              "sewage_cost": {dis: "Sewage Cost", color: colorDict->water},
  
              "elec_ghg":{dis: "Electric GHG", color: colorDict->electric},
              "gas_ghg": {dis: "Gas GHG", color: colorDict->chw},
              "chw_ghg": {dis: "Chilled Water GHG", color: colorDict->chw},
              "hhw_ghg": {dis: "Hot Water GHG", color: colorDict->hhw},
              "steam_ghg": {dis: "Steam GHG", color: colorDict->steam},
              "water_ghg": {dis: "Water GHG", color: colorDict->water},
              "sewage_ghg": {dis: "Sewage GHG", color: colorDict->water},
              }
  
    snapshotRecTag: bldg_mapSelectionsToSnapshotTag(utilityMetric, utilityType)
    spanA: toDateSpan(dates)
  
    //Removing unused tags simplify the debugging. However, total needs more tags. For this reason, the trimming of tags only happen when we are not dealing with total
    bldgSnapshotRecsB:
      if (startsWith(snapshotRecTag, "total"))
         bldgSnapshotRecsA
      else
        bldgSnapshotRecsA.findAll(snapshotA=> snapshotA.has(snapshotRecTag)
                                  and snapshotA.trap(snapshotRecTag).isNumber
                                  and not snapshotA.trap(snapshotRecTag).isNaN)
          .map(snapshotA=> snapshotA.findAll((v, k)=> contains(["monthStart", "siteRef", "area", snapshotRecTag], k)))
  
  
    if(not showPrevYear) do
      if(snapshotRecTag=="total_eui") do
        out:siteIdsA.map (sIdA => do
            dateRecs: bldg_findAllSnapshotsInSiteAndSpan(bldgSnapshotRecsB, sIdA, dates)
            colNeeded: dateRecs.colNames.findAll(cn=> cn.endsWith("_euiCommon"))
            dictRes: {id: sIdA}
            //Iterate to sum and put in the tag. For example, sum all value in tag elec_euiCommon and put the result to elec_eui
            each(colNeeded, colToSum=>do
              colPutIn: colToSum.replace("_euiCommon", "_eui")
              dictRes = merge(dictRes, helper_sumSnapshotRecsAndPutIn(dateRecs, colToSum, colPutIn, {throwErr: false}))
            end)
            dictRes = dictRes.merge(bldg_sumSnapshotRecs(dateRecs, "total_eui", {throwErr: false}))
        end)
  
        gridOut: toGrid(out).sortr("total_eui").removeCol("total_eui")
        colsCheckPositive: gridOut.colNames.findAll(c=> c.endsWith("_eui"))
  
        gridOut = helper_keepColsWhenFoldPositive(gridOut, colsCheckPositive)
        gridOut = helper_addMetaToCols(gridOut, colorAndDis )
        return gridOut= gridOut.addMeta({title:"", chartType:"stackedBar", chartNoScroll})
      end else
  
      if(snapshotRecTag=="total_energy") do
  
        out: siteIdsA.map (sIdA => do
            dateRecs: bldg_findAllSnapshotsInSiteAndSpan(bldgSnapshotRecsB, sIdA, dates)
            colNeeded: dateRecs.colNames.findAll(cn=> cn.endsWith("_energyCommon"))
            dictRes: {id: sIdA}
            //Iterate to sum and put in the tag. For example, sum all value in tag elec_energyCommon and put the result to elec_energy
            each(colNeeded, colToSum=>do
              colPutIn: colToSum.replace("_energyCommon", "_energy")
              dictRes = merge(dictRes, helper_sumSnapshotRecsAndPutIn(dateRecs, colToSum, colPutIn, {throwErr: false}))
            end)
            dictRes = dictRes.merge(bldg_sumSnapshotRecs(dateRecs, "total_energy", {throwErr: false}))
            
        end)
        gridOut: toGrid(out).sortr("total_energy").removeCol("total_energy")
        colsCheckPositive: gridOut.colNames.findAll(c=> c.endsWith("_energy"))
        gridOut = helper_keepColsWhenFoldPositive(gridOut, colsCheckPositive)
        gridOut = helper_addMetaToCols(gridOut, colorAndDis )
        return gridOut= gridOut.addMeta({title:"", chartType:"stackedBar", chartNoScroll})
  
      end else if(snapshotRecTag=="total_cost") do
  
        out:siteIdsA.map (sIdA => do
            dateRecs: bldg_findAllSnapshotsInSiteAndSpan(bldgSnapshotRecsB, sIdA, dates)
            colNeeded: dateRecs.colNames.findAll(cn=> cn.endsWith("_cost"))
            dictRes: {id: sIdA}
            //Iterate to sum and put in the tag. For example, sum all value in tag elec_energyCommon and put the result to elec_energy
            each(colNeeded, colToSum=>do
                dictRes = merge(dictRes, bldg_sumSnapshotRecs(dateRecs, colToSum, {throwErr: false}))
            end)
            dictRes = dictRes.merge(bldg_sumSnapshotRecs(dateRecs, "total_cost", {throwErr: false}))
        end)
  
        gridOut: toGrid(out).sortr("total_cost").removeCol("total_cost")
        colsCheckPositive: gridOut.colNames.findAll(c=> c.endsWith("_cost"))
  
        gridOut = helper_keepColsWhenFoldPositive(gridOut, colsCheckPositive)
        gridOut = helper_addMetaToCols(gridOut, colorAndDis )
        return gridOut= gridOut.addMeta({title:"", chartType:"stackedBar", chartNoScroll})
      end else
        if(snapshotRecTag=="total_ghg") do
  
          out: siteIdsA.map (sIdA => do
              dateRecs: bldg_findAllSnapshotsInSiteAndSpan(bldgSnapshotRecsB, sIdA, dates)
              colNeeded: dateRecs.colNames.findAll(cn=> cn.endsWith("_ghg"))
              dictRes: {id: sIdA}
              //Iterate to sum and put in the tag. For example, sum all value in tag elec_euiCommon and put the result to elec_eui
              each(colNeeded, colToSum=>do
                dictRes = merge(dictRes, bldg_sumSnapshotRecs(dateRecs, colToSum, {throwErr: false}))
              end)
            dictRes = dictRes.merge(bldg_sumSnapshotRecs(dateRecs, "total_ghg", {throwErr: false}))
          end)
  
          gridOut: toGrid(out).sortr("total_ghg").removeCol("total_ghg")
          colsCheckPositive: gridOut.colNames.findAll(c=> c.endsWith("_eui"))
  
          gridOut = helper_keepColsWhenFoldPositive(gridOut, colsCheckPositive)
          gridOut = helper_addMetaToCols(gridOut, colorAndDis )
          gridOut= gridOut.addMeta({title:"", chartType:"stackedBar", chartNoScroll})
        return gridOut
  
  
      end else do
        chartDis: utilityType + " " + utilityMetric
        colorA: colorGrid.find(me => me->utilityType==utilityType)
        colorSelected: if (colorA.has("color")) colorA->color
                      else "#000"
  
        out: siteIdsA.map (sIdA => do
          dateRecs: bldg_findAllSnapshotsInSiteAndSpan(bldgSnapshotRecsB, sIdA, dates)
  
          {id: sIdA}.set(snapshotRecTag, try dateRecs.foldCol(snapshotRecTag, sum) catch null)
        end)
        gridOut: toGrid(out)
        if (gridOut.has(snapshotRecTag)) do
          gridOut = gridOut.addColMeta(snapshotRecTag, {dis: chartDis, color: colorGrid.find(me => me->utilityType==utilityType)->color})
            .reorderCols(["id", snapshotRecTag])
            .addMeta({title:"", chartType:"bar", chartNoScroll, view: "chart"})
            .sortr(snapshotRecTag)
          end
        return gridOut
  
      end
  
    end else if(isBool(showPrevYear) and showPrevYear == true) do
     prevYearSpan: toDateSpan(util_shiftYear(spanA.start, 1year)..util_shiftYear(spanA.end, 1year))
     out: siteIdsA.map (sIdA => do
       dateRecsPrev: bldg_findAllSnapshotsInSiteAndSpan(bldgSnapshotRecsB, sIdA, prevYearSpan)
        dateRecs: bldg_findAllSnapshotsInSiteAndSpan(bldgSnapshotRecsB, sIdA, spanA)
        {id: sIdA,
          prev: try dateRecsPrev.foldCol(snapshotRecTag, sum) catch null,
          selected: try dateRecs.foldCol(snapshotRecTag, sum) catch null}
      end)
      gridOut: toGrid(out).sortr("selected")
  
      if (gridOut.has("prev"))
          gridOut = gridOut.addColMeta("prev", {dis: prevYearSpan.format("YYYY"), color: "#00355f"})
      if (gridOut.has("selected"))
          gridOut = gridOut.addColMeta("selected", {dis: (prevYearSpan.start + 1yr).format("YYYY"), color: "#8cd63f"})
      gridOut = gridOut.addMeta({title:"", chartType:"bar", chartNoScroll})
  
      return gridOut
    end
  end
