name:bldg_viewBldgMeterCharts
bldgPod
doc:
  Present different chart types based on user selection.
  
  Authors:
  - Hali Sanderlin: original author
  - Gia Nguyen
func
src:
  (siteSelection, currDates, reportOptions) => do
    if(reportOptions.isEmpty) do
      mode:              "Raw History (w/ Baseline Comparison)"
      baselineType:      "Baseline Prev Week"
      baselineDates:     yesterday().toSpan
      days:              "all"
      naDataFilter:      "Remove NA Data"
      compareSites:      []
    end else do
      mode:          reportOptions["mode"]
      baselineType:  reportOptions["baselineType"]
      baselineDates: reportOptions["baselineCustomDate"]
      days:          reportOptions["days"]
      naDataFilter:  reportOptions["naDataFilter"]
      compareSites:  reportOptions["compareSiteRefs"]
      navFilterRef:  "bldgEnergyReportRef"
    end
  
    if(compareSites==null)
      compareSites= []
  
    siteSelectionId: toRecId(siteSelection)
  
    selectedPoints: readAll(equipRef->bldg_meterCompare and bldgUtilityReportRef==siteSelectionId)
    if (isEmpty(selectedPoints))
      return "Select a meter point"
  
    colorIndex: 0
    colorVal: bldg_emisReports_colorGen(colorIndex)
  
    comparePointList: bldg_handleOptions_compareSites(compareSites, baselineType)
    baselineInfo: bldg_handleOptions_baselines(baselineType, baselineDates, currDates, selectedPoints)
    baselineDates= baselineInfo[0]
    baselinePoints: baselineInfo[1]
    days= bldg_handleOptions_days(days)
    dayList: days[0]
    dayText: days[1]
  
    if(mode == "Avg Daily Profile") do
      finalHisList: []
      colorIndex= 0
      colorVal= bldg_emisReports_colorGen(colorIndex)
  
      selectedPoints.each (point => do
  
        pointHistory: bldg_getAvgProfile(point, currDates, naDataFilter, dayList)
        try
          pointHistory= pointHistory.addColMeta("v0", {strokeWidth: 2, color: colorVal, dis: readById(siteSelection)->id.refDis})
        catch null
  
        if(pointHistory.size > 0)
          finalHisList= finalHisList.add(pointHistory)
  
        colorIndex= colorIndex + 1
        colorVal= bldg_emisReports_colorGen(colorIndex)
      end)
  
      comparePointList.each (row => do
        colorIndex= colorIndex + 1
        colorVal= bldg_emisReports_colorGen(colorIndex)
        endUseType: bldg_getEndUseType(row)
        compareHistory: bldg_getAvgProfile(row, currDates, naDataFilter, dayList)
        try do
          compareHistory= compareHistory.addColMeta("v0", {dis: row.get(navFilterRef).refDis + " \u25CF " + endUseType, color: colorVal, strokeWidth: 2})
          finalHisList= finalHisList.add(compareHistory)
        end
        catch null
      end)
  
      try
        finalHisList= finalHisList.hisJoin.addMeta({chartLegendNoSort: marker(), title: " \u2022 Daily Average" + " \u2022 (" + dayText + ")"})
      catch
        finalHisList= null
  
      return finalHisList
    end
  
    //-----------------------------------------
    if(mode == "Raw History") do
      finalHisList: []
      colorIndex= 0
      colorVal= bldg_emisReports_colorGen(colorIndex)
  
      selectedPoints.each (point => do
        endUseType: bldg_getEndUseType(point)
        pointHistory: point.hisRead(currDates, {-limit}).hisRollup(avg, 1hr)
        if(naDataFilter == "Remove NA Data")
          pointHistory= point.hisRead(currDates, {-limit}).hisMap(val => if(val == na()) val = null else val).hisRollup(avg, 1hr)
  
        try do
          pointHistory= pointHistory.addColMeta("v0", {strokeWidth: 1, color: colorVal, dis: readById(siteSelection)->id.refDis + " \u25CF " + endUseType})
          finalHisList= finalHisList.add(pointHistory)
        end catch null
        colorIndex= colorIndex + 1
        colorVal= bldg_emisReports_colorGen(colorIndex)
      end)
  
      comparePointList.each (row => do
        colorIndex=  colorIndex + 1
        colorVal= bldg_emisReports_colorGen(colorIndex)
        endUseType: bldg_getEndUseType(row)
        compareHistory: row.hisRead(currDates, {-limit}).hisRollup(avg, 1hr)
        if(naDataFilter == "Remove NA Data")
          compareHistory=  row.hisRead(currDates, {-limit}).hisMap(val => if(val == na()) val = null else val).hisRollup(avg, 1hr)
  
        try do
          compareHistory= compareHistory.addColMeta("v0", {dis: row.get(navFilterRef).refDis + " \u25CF " + endUseType, color: colorVal, strokeWidth: 1})
          finalHisList= finalHisList.add(compareHistory)
        end catch null
      end)
  
      try   finalHisList= finalHisList.hisJoin.addMeta({chartLegendNoSort: marker(), title: "Raw History"})
      catch finalHisList= null
  
      return finalHisList
    end
  
    //-----------------------------------------
    if(mode == "Raw History (w/ Baseline Comparison)") do
      finalHisList: []
      reportText: ""
      colorIndex= 0
      colorVal= bldg_emisReports_colorGen(colorIndex)
  
      toRecList(selectedPoints).map (point => do
        endUseType: bldg_getEndUseType(point)
        pointHistory: point.hisRead(currDates, {-limit}).hisRollup(avg, 1hr)
        if(naDataFilter == "Remove NA Data")
          pointHistory=  point.hisRead(currDates, {-limit}).hisMap(val => if(val == na()) val = null else val).hisRollup(avg, 1hr)
        try do
          pointHistory= pointHistory.addColMeta("v0",
            {strokeWidth: 2, color: colorVal, dis: readById(siteSelectionId)->id.refDis + " \u25CF " + endUseType})
          finalHisList= finalHisList.add(pointHistory)
          //reportText= reportText + pointHistory.meta.dis
        end catch (err)
          err
        pointHistory
        colorIndex= colorIndex + 1
        colorVal= bldg_emisReports_colorGen(colorIndex)
      end)
  
  
      if(reportText == "")
        //reportText= reportText + currDates.toStr
  
      if(baselineType == "Predefined Baseline") do
        baselinePoints.each (row => do
          baselineTempHis: row.hisRead(currDates, {-limit}).hisRollup(avg, 1hr)
          if(naDataFilter == "Remove NA Data")
            baselineTempHis=  row.hisRead(currDates, {-limit}).hisMap(val => if(val == na()) val = null else val).hisRollup(avg, 1hr)
          try do
            baselineTempHis= baselineTempHis.addColMeta("v0", {color: colorVal, strokeWidth: 1, strokeDasharray: "6,4", dis: row->id.refDis + " (" + baselineTempHis.meta.dis + ")"})
            finalHisList= finalHisList.add(baselineTempHis)
          end catch null
        end)
  
        reportText= reportText + " vs. Predefined Baseline(s)"
  
      end else do
       metaCapture: {}
       colorIndex= 0
       colorVal= bldg_emisReports_colorGen(colorIndex)
  
       baselineDiff: (currDates.start.day - baselineDates.start.day).to("day")
        selectedPoints.each (point => do
          baselineHistory: point.hisRead(baselineDates, {-limit}).map(row => {ts: row->ts + baselineDiff, v0: row["v0"]}).hisRollup(avg, 1hr)
          if(naDataFilter == "Remove NA Data")
            baselineHistory=  point.hisRead(baselineDates, {-limit}).hisMap(val => if(val == na()) val = null else val).hisRollup(avg, 1hr).map(row => {ts: row->ts + baselineDiff, v0: row["v0"]})
          try do
            baselineHistory= baselineHistory.addColMeta("v0", {strokeWidth: 1, strokeDasharray: "6,4", color: colorVal, dis: readById(siteSelection)->id.refDis + " - Baseline (" + baselineHistory.meta.dis + ")"})
            finalHisList= finalHisList.add(baselineHistory)
            metaCapture= baselineHistory.meta.dis
          end catch null
          colorIndex= colorIndex + 1
          colorVal= bldg_emisReports_colorGen(colorIndex)
        end)
        reportText= reportText + " vs. Baseline (" + metaCapture + ")"
      end
  
      comparePointList.each (row => do
        colorIndex= colorIndex + 1
        colorVal= bldg_emisReports_colorGen(colorIndex)
        endUseType: bldg_getEndUseType(row)
        compareHistory: row.hisRead(currDates, {-limit})
        if(naDataFilter == "Remove NA Data")
            compareHistory=  row.hisRead(currDates, {-limit}).hisMap(val => if(val == na()) val = null else val).hisRollup(avg, 1hr)
        try do
          compareHistory= compareHistory.addColMeta("v0", {dis: row.get(navFilterRef).refDis + " \u25CF " + endUseType, color: colorVal, strokeWidth: 2})
          finalHisList= finalHisList.add(compareHistory)
        end catch null
  
        if(baselineType == "Predefined Baseline") do
          emisBaselinePoints: row["emisBaselinePoints"]
          if(emisBaselinePoints != null) do
            emisBaselinePoints.each row2 => do
              baselineTempHis: row2.hisRead(currDates, {-limit})
              if(naDataFilter == "Remove NA Data")
                baselineTempHis=  row2.hisRead(currDates, {-limit}).hisMap(val => if(val == na()) val = null else val).hisRollup(avg, 1hr)
              try do
                baselineTempHis= baselineTempHis.addColMeta("v0", {color: colorVal, strokeWidth: 1, strokeDasharray: "6,4", dis: row2->id.refDis + "(" + baselineTempHis.meta.dis + ")"})
                finalHisList= finalHisList.add(baselineTempHis)
              end catch null
            end
          end
        end else do
          baselineHistory: row.hisRead(baselineDates, {-limit})
          if(naDataFilter == "Remove NA Data")
                baselineHistory=  row.hisRead(baselineDates, {-limit}).hisMap(val => if(val == na()) val = null else val).hisRollup(avg, 1hr).map(row => {ts: row->ts + baselineDiff, v0: row["v0"]})
          try do
            baselineHistory= baselineHistory.addColMeta("v0", {strokeWidth: 1, strokeDasharray: "6,4", color: colorVal, dis: row.get(navFilterRef).refDis + " - Baseline (" + baselineHistory.meta.dis + ")"})
            finalHisList= finalHisList.add(baselineHistory)
          end catch null
        end
      end)
  
      if(finalHisList.isEmpty == false)
        finalHisList= finalHisList.hisJoin
  
      //chart formatting
      try do
        finalHisList= finalHisList.addMeta({chartLegendNoSort: marker(), title: "Raw History" + " \u2022 " + reportText + " \u2022 (" + dayText + ")"})
      end catch
        finalHisList= null
      if (finalHisList!=null)
        return finalHisList.addMeta({hisStart: currDates.start, hisEnd:currDates.end})
      else
        return null
    end
  
    //-----------------------------------------
    if(mode == "Avg Daily Profile (w/ Baseline Comparison)") do
      finalHisList: []
      reportText: ""
      colorIndex= 0
      colorVal= bldg_emisReports_colorGen(colorIndex)
  
      selectedPoints.each (point => do
        pointHistory: bldg_getAvgProfile(point, currDates, naDataFilter, dayList)
        try do
          pointHistory= pointHistory.addColMeta("v0", {strokeWidth: 2, color: colorVal, dis: readById(siteSelection)->id.refDis})
          finalHisList= finalHisList.add(pointHistory)
          reportText= reportText + pointHistory.meta.dis
        end catch null
        colorIndex= colorIndex + 1
        colorVal= bldg_emisReports_colorGen(colorIndex)
      end)
  
      if(reportText == "")
        reportText= reportText + currDates.toStr
  
      if(baselineType == "Predefined Baseline") do
        baselinePoints.each (row => do
          baselineTempHis: bldg_getAvgProfile(row, row["hisStart"].date..row["hisEnd"].date, naDataFilter, dayList)
          try do
            baselineTempHis= baselineTempHis.addColMeta("v0", {color: colorVal, strokeWidth: 1, strokeDasharray: "6,4", dis: row->id.refDis + " (" + baselineTempHis.meta.dis + ")"})
            finalHisList= finalHisList.add(baselineTempHis)
          end catch null
        end)
  
        reportText= reportText + " vs. Predefined Baseline(s)"
      end else do
  
        selectedPoints.each (point => do
          baselineHistory: bldg_getAvgProfile(point, baselineDates, naDataFilter, dayList)
          try do
            baselineHistory= baselineHistory.addColMeta("v0", {strokeWidth: 1, strokeDasharray: "6,4", color: colorVal, dis: readById(siteSelection)->id.refDis + " - Baseline (" + baselineHistory.meta.dis + ")"})
            finalHisList= finalHisList.add(baselineHistory)
            reportText= reportText + " vs. Baseline (" + baselineHistory.meta.dis + ")"
          end catch null
          colorIndex= colorIndex + 1
          colorVal= bldg_emisReports_colorGen(colorIndex)
        end)
      end
  
      comparePointList.each (row => do
        colorIndex= colorIndex + 1
        colorVal= bldg_emisReports_colorGen(colorIndex)
        endUseType: bldg_getEndUseType(row)
        compareHistory: bldg_getAvgProfile(row, currDates, naDataFilter, dayList)
        try do
          compareHistory= compareHistory.addColMeta("v0", {dis: row.get(navFilterRef).refDis + " \u25CF " + endUseType, color: colorVal, strokeWidth: 2})
          finalHisList= finalHisList.add(compareHistory)
        end catch null
  
        if(baselineType == "Predefined Baseline") do
          emisBaselinePoints: row["emisBaselinePoints"]
          if(emisBaselinePoints != null) do
            emisBaselinePoints.each row2 => do
              baselineTempHis: bldg_getAvgProfile(row2, row2["hisStart"]..row2["hisEnd"], naDataFilter, dayList)
              try do
                baselineTempHis= baselineTempHis.addColMeta("v0", {color: colorVal, strokeWidth: 1, strokeDasharray: "6,4", dis: row2->id.refDis + "(" + baselineTempHis.meta.dis + ")"})
                finalHisList= finalHisList.add(baselineTempHis)
              end catch null
            end
          end
        end else do
          baselineHistory: bldg_getAvgProfile(row, baselineDates, naDataFilter, dayList)
          try do
            baselineHistory= baselineHistory.addColMeta("v0", {strokeWidth: 1, strokeDasharray: "6,4", color: colorVal, dis: row.get(navFilterRef).refDis + " - Baseline (" + baselineHistory.meta.dis + ")"})
            finalHisList= finalHisList.add(baselineHistory)
          end catch null
        end
      end)
  
      if(finalHisList.isEmpty == false)
        finalHisList= finalHisList.hisJoin
  
      //chart formatting
      try do
        finalHisList= finalHisList.addMeta({chartLegendNoSort: marker(), title: "Daily Average" + " \u2022 " + reportText + " \u2022 (" + dayText + ")"})
      end catch
        finalHisList= null
  
      return finalHisList
    end
  
    //-----------------------------------------
    if(mode == "Avg Daily Profile By Weekday") do
  
      finalGrid: []
  
      selectedPoints.each (point => do
        pointHistory: point.hisRead(currDates, {-limit})
        if(naDataFilter == "Remove NA Data")
          pointHistory=  pointHistory.hisMap(val => if(val == na()) val = null else val)
  
        pointHistory= pointHistory.hisRollup(avg, 15min)
  
        dayList.each dayVal => do
          dayHistory: pointHistory.hisFindAll((val, ts) => dayVal == ts.weekday)
          dayHistory= dayHistory.hisDailyProfile(avg)
  
          if(dayVal == 0day) dayDisText: "Sunday"
          if(dayVal == 1day) dayDisText: "Monday"
          if(dayVal == 2day) dayDisText: "Tuesday"
          if(dayVal == 3day) dayDisText: "Wednesday"
          if(dayVal == 4day) dayDisText: "Thursday"
          if(dayVal == 5day) dayDisText: "Friday"
          if(dayVal == 6day) dayDisText: "Saturday"
          try do
            dayHistory=  dayHistory.addColMeta("v0", {dis: dayDisText})
            if(finalGrid.isEmpty)
              finalGrid= dayHistory
            else
              finalGrid= hisJoin([finalGrid, dayHistory])
          end catch null
        end
      end)
  
      try
        finalReturnGrid: finalGrid.addMeta({chartLegendNoSort: marker(), title: readById(siteSelection)->id.refDis + " \u2022 Daily Average by Weekday" + " \u2022 (" + dayText + ")"})
      catch
        finalReturnGrid: null
  
      return finalReturnGrid
    end
  end
  
