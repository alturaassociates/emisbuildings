#! /usr/bin/env fan
 //
// Copyright 2022, Altura Associates
// All Rights Reserved
//
// History:
//   11 Nov 19   HaliSanderlin   Creation
//   18 Nov 19   Hali Sanderlin  v0.1.6      Pod after troubleshooting of initial UCI install.
//   22 Nov 19   Hali Sanderlin  v0.1.7      Updates, ready for client demo.  New base to start next feature dev.
//   27 Jan 20   Hali Sanderlin  v0.2.0/1    First beta
//   03 Feb 20   Hali Sanderlin  v1.0        First production release and axonated
//   09 Mar 20   Hali Sanderlin  v1.0.1      Fixed axonated duplicate, minor feature updates
//   04 Jun 20   Hali Sanderlin  v1.0.2      Hot fix for duplicate snapshot recs
//   01 Sep 21   Hali Sanderlin  v1.1        UCSF Reqmts: add water, add ghg
//   Feb 22      Alex Leroux  v1.2           Put on the version Hali has on local computer    
//   05 March 22   Gia Nguyen  v1.3          Add steam
//   16 March 22   Gia Nguyen  v2.0          Update for SkySpark 3.1.2
using build

**
** Build: alturaEmisBuildingsExt
**
class Build : BuildPod
{
  new make()
  {
    podName = "alturaEmisBuildingsExt"
    summary = "EMIS Building Views"
    version = Version("2.0.4.6")
    meta    = [
                "ext.name":    "alturaEmisBuildingsExt",
                "ext.depends": "alturaUtilityFunc, alturaMeasAndVerif, sfxCalculus, alturaEmisReportsExt, afAxonEncryptorRt, point",
                "org.name":    "Altura Associates",
                "license.name": "Commercial",
              ]
    depends = ["sys 1.0","skyarc 3.1+","skyarcd 3.1+","afAxonEncryptorRtExt 2.0+"]
    resDirs = [`locale/`,
               `lib/`]
    index   =
    [
      "skyarc.ext": "alturaEmisBuildingsExt",
    ]
  }
}
